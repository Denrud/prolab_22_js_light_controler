/*
ДЗ. 
четыре кнопки. 2 кнопки - яркость + /- от 0 до %. 2 кнопки - цветовая температура в кельвинах + / -
рядом две цифры - явкость и температура.
реализовать алгоритм по принципам MVC

*/
//model
const lighterData = {
    kelvinScale: [
        {scale: '1000', colorClass: 'kelv-1000'},
        {scale: '2000', colorClass: 'kelv-2000'},
        {scale: '3000', colorClass: 'kelv-3000'},
        {scale: '4000', colorClass: 'kelv-4000'},
        {scale: '5000', colorClass: 'kelv-5000'},
        {scale: '6000', colorClass: 'kelv-6000'},
        {scale: '7000', colorClass: 'kelv-7000'},
        {scale: '8000', colorClass: 'kelv-8000'},
        {scale: '9000', colorClass: 'kelv-9000'},
        {scale: '10000', colorClass: 'kelv-10000'}
    ],
    lightParams: {
        state: false,
        voltage: {nominal: '220'},
        wattage: {min: '0', max: '300'},
        status: [
            {voltage: '0'},
            {wattage: '0'},
            {scaleIndex: '0', colorClass: '000000'}
        ]
    },
}

// controller
const spans = document.querySelectorAll('span');
const buttons = document.querySelectorAll('button');
const kelvTempBG = document.querySelector('.glass-lamp');
let state = lighterData.lightParams.state ;
console.log(state);
let plug = lighterData.lightParams.status.find(item => item.voltage);
let power = lighterData.lightParams.status.find(item => item.wattage);
let kelv = lighterData.lightParams.status.find(item => item.scaleIndex);
console.log(kelv);
let countLighterMod = lighterData.kelvinScale.length;
let lightColor = lighterData.lightParams.status.find(item => item.colorClass);

buttons.forEach(el => el.addEventListener('click', (ev) => {
    if ( ev.target.dataset.att === 'powerBtn') {       
        powerBtn();
        powerController();
        lightColorCode();
        statusBlock();
        return; 
    }
    if ( ev.target.dataset.att === 'kelUp' && plug.voltage === '220' ) {
        kelvUp();
        powerController();
        lightColorCode();
        statusBlock();
        return;
    }
    if ( ev.target.dataset.att === 'kelDown' && plug.voltage === '220' ) {
        kelvDown();
        powerController();
        lightColorCode();
        statusBlock();
        return;
    }
    if ( ev.target.dataset.att === 'powerUp' && plug.voltage === '220' ) {
        powerUp();
        powerController();
        lightColorCode();
        statusBlock();
        return;
    }
    if ( ev.target.dataset.att === 'powerDown' && plug.voltage === '220' ) {
        powerDown();
        powerController();
        lightColorCode();
        statusBlock();
        return;
    }

}));

const powerBtn = () => {
    if ( state !== true ) {
        state = lighterData.lightParams.state = true;
        kelv.scaleIndex = 0;
        lightColor.colorClass = lighterData.kelvinScale[kelv.scaleIndex].colorClass;
    }
    else if ( state !== false) {
        state = lighterData.lightParams.state = false;
    }
    
}

const powerController = () => {
    if ( state !== false ) { 
        power.wattage = lighterData.lightParams.wattage.max / (countLighterMod - kelv.scaleIndex)  ;
        plug.voltage = lighterData.lightParams.voltage.nominal;
    }
    else if ( state !== true ) {
        power.wattage = 0;
        plug.voltage = 0;
    }
}

const lightColorCode = () => {  
    console.log(plug.voltage)
    if ( plug.voltage === '220') {
        kelvTempBG.classList.remove('black');
        kelvTempBG.classList.add(`${lighterData.kelvinScale[kelv.scaleIndex].colorClass}`);
        
    }
    else if ( plug.voltage === 0 ) {
        kelvTempBG.classList.remove(`${lighterData.kelvinScale[kelv.scaleIndex].colorClass}`);
        kelvTempBG.classList.add('black');
        kelv.scaleIndex = 0;
    }
};

const kelvUp = () => {
    kelvTempBG.classList.remove(`${lighterData.kelvinScale[kelv.scaleIndex].colorClass}`);
    let up = Number(kelv.scaleIndex);
    if ( up < countLighterMod - 1 ) {
        up += 1;
    }
    const res = kelv.scaleIndex = up;  
}

const kelvDown = () => {
    kelvTempBG.classList.remove(`${lighterData.kelvinScale[kelv.scaleIndex].colorClass}`);
    let down = Number(kelv.scaleIndex);
    if ( down > 0 ) {
        down -= 1;
    }
    const res = kelv.scaleIndex = down;
}

const powerUp = () => {
    kelvTempBG.classList.remove(`${lighterData.kelvinScale[kelv.scaleIndex].colorClass}`);
    let up = Number(kelv.scaleIndex);
    if ( up < countLighterMod - 1 ) {
        up += 1;
    }
    const res = kelv.scaleIndex = up;  
}

const powerDown = () => {
    kelvTempBG.classList.remove(`${lighterData.kelvinScale[kelv.scaleIndex].colorClass}`);
    let down = Number(kelv.scaleIndex);
    if ( down > 0 ) {
        down -= 1;
    }
    const res = kelv.scaleIndex = down;
}


//view
const statusBlock = () => {
    if ( plug.voltage && lighterData.lightParams.state !== false ) {
        spans.forEach( (objDOM) => {
            if ( objDOM.classList.value === 'status') {
                objDOM.innerText = 'on';
            }  
            if ( objDOM.classList.value === 'voltage' ) {
                objDOM.innerText = lighterData.lightParams.voltage.nominal; 
            }
            if ( objDOM.classList.value === 'wattage' ) {
                objDOM.innerText = Math.round(power.wattage);
            }
            if ( objDOM.classList.value === 'kelvin' ) {
                objDOM.innerText = lighterData.kelvinScale[kelv.scaleIndex].scale;
            }
        });
    }
    else if ( lighterData.lightParams.state !== true ) {
        spans.forEach( (objDOM) => {
            if ( objDOM.classList.value === 'status') {
                objDOM.innerText = 'off';
            }  
            if ( objDOM.classList.value === 'voltage' ) {
                objDOM.innerText = plug.voltage; 
            }
            if ( objDOM.classList.value === 'wattage' ) {
                objDOM.innerText = power.wattage;
            }
            if ( objDOM.classList.value === 'kelvin' ) {
                objDOM.innerText = kelv.scaleIndex;
            }
        });
    } 
};

